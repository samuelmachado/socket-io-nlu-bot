var genericos = require('./genericos.js');


const cf1_1 = [
'Tudo bem',
'Sem problema',
'Ok',
'Certo',
'Entendi',
'Perfeito',
];

const cf1_2 = [
'!!! ',
'!! ',
'! ',
'. ',
'.. ',
'... ',
', ',
];


const cf1_3 = [
'mas se souber de alguém ',
'quando precisar me procure ',
'é uma excelente opção. Quando precisar me procure ',
'é uma excelente opção. Se souber de alguém ',
];


const cf1_4 = [
"eu posso ajudar",
"nós podemos ajudar",
"fico à sua disposição",
"estou à sua disposição",
];

const cf1_5 = [
'!! ',
'!!!',
'!!',
'! ',
'.. ',
'... ',
'..',
'.',
' ',
];


module.exports = {
cf1_1,
cf1_2,
cf1_3,
cf1_4,
cf1_5,
}