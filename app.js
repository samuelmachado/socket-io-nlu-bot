var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);

var clients = {}; 
//dialogs
var genericos = require('./dialogs/genericos.js');
var ads = require('./dialogs/anuncio.js');
//dialogs


const { NlpManager } = require('node-nlp');
const _ = require('lodash');

const fs = require('fs');
const data = fs.readFileSync('model.nlp', 'utf8');
const manager = new NlpManager();
manager.import(data);


const bodyParser = require('body-parser');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-auth");
  res.header("Access-Control-Expose-Headers", "true");
  res.header("Access-Control-Allow-Credentials","true");
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH');
  next();
});

var getRand = (arr) => {
    return arr[Math.floor(Math.random()*arr.length)];
}

var seconds = 0;

var genAds = () => {
 return getRand(genericos.apres) +
        getRand(genericos.ligacao) +
        getRand(genericos.nomesFem) +
        getRand(genericos.pontoPosNome) +
        getRand(ads.ad1_1) +
        getRand(ads.ad1_2) +
        getRand(ads.ad1_3) +
        getRand(ads.ad1_4) +
        getRand(ads.ad1_5) +
        getRand(ads.ad1_6) +
        getRand(ads.ad1_7) +
        getRand(ads.ad1_8) +
        getRand(ads.ad1_9);
}
var randSeconds = (start, end) => {
    return (Math.floor((Math.random() * end) + start) );
}
var miliSeconds = (seconds) => {
    return seconds* 1000; 
}
var isInArray = (value, array) => {
  return array.indexOf(value) > -1;
}
var cleanText = (text) => {
   return text.normalize('NFD').replace(/[\u0300-\u036f]/g, "").toLowerCase().trim().replace(/[^\w\s]/gi, '');
}

app.get('/', function(req, res){
  res.send('server is running');
});

app.post('/process', (req, res) => {
    var body = _.pick(req.body, ['text', 't']);
    (async() => {
        const response = await manager.process('pt', body.text);
        res.status(200).send(response);
    })();
});

io.on("connection", function (client) {  
    client.on("join", function(name){
    	console.log("Joined: " + name);
        clients[client.id] = name;
        client.emit("update", "Imagine que você recebeu a mensagem abaixo. A partir da sua resposta o bot vai classificar a sua intenção com base na mensagem original e na resposta.");
        let msg = genAds();
        console.log(msg);
        client.emit("update", msg);
        client.broadcast.emit("update", name + " entrou no servidor.")
    });

    client.on("send", function(msg){
    	console.log("Message: " + msg);
        client.broadcast.emit("chat", clients[client.id], msg);
        (async() => {
            const response = await manager.process('pt', cleanText(msg));
            console.log(response); 
            if(response.intent == 'None') {
                client.emit("update", 'Ainda não fui treinada para responder o que você escreveu. Tente novamente!');
            } else {
                client.emit("update", 'Detectado: '+response.intent+' com '+(response.score*100).toFixed(2)+'%');
            }
            
        })();
        
    });

    client.on("disconnect", function(){
    	console.log("Disconnect");
        io.emit("update", clients[client.id] + " saiu do servidor.");
        delete clients[client.id];
    });
});


http.listen(3000, function(){
  console.log('listening on port 3000');
});

